
window.onload = function() {
  let ringBuffer = new RingBuffer();
  //init();

  setTimeout(updateFrame, 1000);
  setTimeout(updateFrame, 2000);
  setTimeout(updateFrame, 3000);
  setTimeout(updateFrame, 4000);
  setTimeout(updateFrame, 5000);
  setTimeout(updateFrame, 6000);

  function init()
  {
    //window.requestAnimationFrame(updateFrame());
    //updateFrame();
  }

  function updateFrame()
  {
    console.log("hello");
    let canvas = document.getElementById("canvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, 300, 300); //x, y, w, h
    //ctx.save();
    //
    ctx.save();
    for(let i = 0; i < 10; i++)
    {
      ctx.font = '25px Ubuntu';
      ctx.fillText(ringBuffer.list[i], 10, 10, 30);
      ctx.translate(0, 30);
    }
    ctx.restore();
    for(let i = 0; i < 10; i++)
    {
      ctx.font = '25px Ubuntu';
      ctx.fillText(ringBuffer.list[i], 30, 10, 30);
      ctx.translate(0, 30);
    }
    ctx.restore();
  

    ringBuffer.shift();
    ringBuffer.log();
    //window.requestAnimationFrame(updateFrame());
  }

};


function drawWord(x, y)
{
  ctx.fillText('A', x, y);
  //ctx.fillText('R', 10, 60);
  //ctx.fillText('C', 10, 70);
  //ctx.fillText('H', 10, 50);
  //
  //ctx.fillText('L', 10, 50);
  //ctx.fillText('I', 10, 50);
  //ctx.fillText('N', 10, 50);
  //ctx.fillText('U', 10, 50);
  //ctx.fillText('X', 10, 50);
}

class RingBuffer
{
  constructor()
  {
    this.list = ['a', 'b', 'c', 'd', 'e', ' ', ' ', '1', '2', '3'];
  }
  
  log()
  {
    for(let i of this.list)
    {
      console.log(i);
    }
    console.log("----");
  }

  shift()
  {
    this.list.unshift(this.list.pop());
  }

  getElementByIndex(i)
  {
    return this.list[i];
  }
   
}

